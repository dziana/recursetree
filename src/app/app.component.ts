import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nodes = [{
    id: '1',
    name: '1',
    showChildren: true,
  }];

  onSelected(event) {
    console.log(event);
  }
}
