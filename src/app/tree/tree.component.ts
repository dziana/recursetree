import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TreeNode} from '../tree-node.model';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class TreeComponent implements OnInit {

  @Input() treeData: TreeNode[];
  @Output() selected = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onSelectItem(event) {
    this.selected.emit(event);
  }

}
