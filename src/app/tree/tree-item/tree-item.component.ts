import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TreeNode} from '../../tree-node.model';

@Component({
  selector: 'app-tree-item',
  templateUrl: './tree-item.component.html',
  styleUrls: ['./tree-item.component.css']
})
export class TreeItemComponent implements OnInit {

  @Input() node: TreeNode;
  @Output() selectedItem = new EventEmitter();
  spinner = false;
  isOpenChild = false;

  constructor() { }

  ngOnInit() {}

  openNode() {
    this.spinner = true;
    setTimeout(() => {
      this.node.children = [
        {
          name: this.node.name + '.1',
          showChildren: true
        },
        {
          name: this.node.name + '.2',
          showChildren: false
        }
      ];
      this.spinner = false;
      this.isOpenChild = true;
    }, 2000);
  }

  closeNode() {
    this.isOpenChild = false;
  }

  selectNode(node) {
    console.log(node.name);
    this.selectedItem.emit(node.name);
  }
}
